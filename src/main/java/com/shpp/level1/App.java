package com.shpp.level1;
//libs

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.Properties;

/**
 * App class creating file .json or .xml by value from nameMessage.properties
 */
public class App {
    /**
     * Logger
     */
    static Logger log = LoggerFactory.getLogger(App.class);

    /**
     * Call loadProperties(); to start program.
     *
     * @param args arguments from console, but this arguments don`t used on code.
     * @throws IOException exception on situation when we cannot find a file.
     */
    public static void main(String[] args) throws IOException {
        log.info("main start working.");
        loadProperties();
    }

    /**
     * Loading our properties and going throw if else to create .json or .xml file.
     *
     * @throws IOException if we cannot find file with properties we will have exception
     */
    private static void loadProperties() throws IOException {
        log.info("\"loadProperties()\" start working.");
        Properties properties = new Properties();
        String xmlOrJsonFormat = System.getProperty("XML_OR_JSON").toLowerCase();
        try {
            properties.load(App.class.getClassLoader().getResourceAsStream("prop.properties"));
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        log.info("properties loads successful." + "user set: ." + xmlOrJsonFormat + " format.");
        if (!xmlOrJsonFormat.equals("xml") && !xmlOrJsonFormat.equals("json")) {
            log.info("User entry value which we can`t process");
            log.error(String.valueOf(new PropertiesIsEmptyException("XML_OR_JSON")));
        } else {
            CreateXmlOrJson.createMessageXMLJSON(xmlOrJsonFormat, properties.getProperty("TEXT_FROM_PROPERTIES"),
                    properties.getProperty("username"));
            log.info("THE END.");
        }
    }
}