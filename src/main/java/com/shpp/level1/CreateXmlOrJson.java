package com.shpp.level1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.IOException;

public class CreateXmlOrJson {
    /**
     * Logger
     */
    static Logger log = LoggerFactory.getLogger(CreateXmlOrJson.class);

    /**
     * creating .xml or .json file with values from .properties
     *
     * @param text_from_properties message from properties
     * @param username             username from properties
     * @throws IOException exception
     */
    public static void createMessageXMLJSON(String format_of_file, String text_from_properties, String username)
            throws IOException {
        if (text_from_properties.equals("")) {
            log.error(String.valueOf(new PropertiesIsEmptyException("text_from_properties")));
        } else if (username.equals("")) {
            log.error(String.valueOf(new PropertiesIsEmptyException("username")));
        } else {
            ObjectMapper mapper;
            if(format_of_file.equals("xml")) {
                mapper = new XmlMapper();
            } else  {
                mapper = new ObjectMapper();
            }
            Message message = new Message();
            message.setMessage(text_from_properties + ", " + username);
            log.info(mapper.writeValueAsString(message));
        }
    }
}
