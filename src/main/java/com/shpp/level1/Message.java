package com.shpp.level1;

/**
 * Create an object with info from properties.
 */
public class Message {
    //Our message.
    String message;

    /**
     * Return message.
     * @return string.
     */
    @SuppressWarnings("unused")
    public String getMessage() {
        return message;
    }

    /**
     * Set sting to our object.
     * @param message string.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
