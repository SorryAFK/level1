package com.shpp.level1;

/**
 * my exception.
 */
public class PropertiesIsEmptyException extends Exception {
    /**
     * This exception need to show where we have empty properties.
     *
     * @param message info about properties.
     */
    public PropertiesIsEmptyException(String message) {
        super("We cannot add empty properties on XML or JSON. So chek >" + message + " and try again");
    }
}